import React from 'react'
import logo from './logo.svg';
import './App.css';

function App() {
    const [serverData, setServerData] = React.useState('');
    const callServer = () => fetch(`${process.env.REACT_APP_SERVER_URL}/test`).then(response => response.text()).then(data => setServerData(data))
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.js</code> and save to reload.
        </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
        </a>
            </header>
            <div style={{ align: 'center' }}><button type="button" onClick={callServer}>Call server</button></div>
            <pre>{serverData}</pre>
        </div>
    );
}

export default App;
